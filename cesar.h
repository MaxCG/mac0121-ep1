#include <stdio.h>
#define MAX 5

typedef struct{
	char ** pointer;
	int tam;
	int allocated;
	int offMin;
	int offMai;
} cesar;

/* Memoria */
cesar alocaCesar();
void destroiCesar(cesar * a);
void realocaCesar(cesar * a);

/* IO */
void leChar(FILE * arq, cesar * a);

/* Helpers */
int deltaChar(char a);
void substituiChar(char * str, int * pos, char ** vec);

/* Main Functions*/
void charEspeciais(cesar * txt);
void offsetChars(cesar * txt);

