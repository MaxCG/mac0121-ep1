#include <stdio.h>
#include <stdlib.h>
#include "cesar.h"

int main(){
	char nome[80];
	FILE * arq;
	cesar txt;

	printf("Digite o nome do arquivo que sera criptografado:\n");
	scanf("%s", nome);

	arq = fopen(nome, "r");

	if (arq == NULL){
		printf("Arquivo nao encontrado\n");
		return 1;
	}


	txt = alocaCesar();

	printf("Digite o offset dos maiusculos\n");
	scanf("%d", &txt.offMai);

	printf("Digite o offset dos minusculos\n");
	scanf("%d", &txt.offMin);

	if(txt.offMai < 0)
		txt.offMai = txt.offMai % 26 + 26;

	if(txt.offMin < 0)
		txt.offMin = txt.offMin % 26 + 26;

	while (!feof(arq)){
		leChar(arq, &txt);
	}


	charEspeciais(&txt);

	offsetChars(&txt);

	printf("%s\n", *txt.pointer);

	fclose(arq);
	destroiCesar(&txt);
	return 0;
}
