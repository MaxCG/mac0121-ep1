#include "cesar.h"
#include <stdlib.h>
#include <stdio.h>

cesar alocaCesar(){
	cesar a;
	int i;

	a.pointer = malloc(sizeof(char *));
	*a.pointer = malloc(MAX * sizeof(char));
	for(i = 0; i < MAX; i++)
		(*a.pointer)[i] = '\0';
	a.allocated = MAX;
	a.tam = 0;
	a.offMai = 0;
	a.offMin = 0;

	return a;
}

void destroiCesar(cesar * a){
	free(*a->pointer);
	free(a->pointer);
}

void realocaCesar(cesar *a){
	char * newArr;
	int i;
	newArr = malloc(2 * a->allocated * sizeof(char));
	for(i = 0; i < 2 * a->allocated; i++)
		newArr[i] = '\0';

	for(i = 0; i < a->tam; i++){
		newArr[i] = (*a->pointer)[i];
	}

	free(*a->pointer);
	a->allocated *= 2;
	*a->pointer = newArr;
}

void leChar(FILE * arq, cesar * a){
	if(a->allocated == a->tam){
		realocaCesar(a);
	}

	fscanf(arq, "%c", &((*a->pointer)[a->tam]));
	a->tam++;
}

int deltaChar(char a){
	if (a == ',' ||
			a =='.' ||
			a ==':' ||
			a =='!' ||
			a =='?' ||
			a == '\n'){
		return 1;
	}else if (a == ' '){
		return -1;
	}else{
		return 0;
	}
}

void substituiChar(char * str, int * pos, char ** vec){
	int i = 0;
	while(str[i] != '\0'){
		(*vec)[*pos] = str[i];
		(*pos)++;
		i++;
	}
}

void charEspeciais(cesar * txt){
	int tamFinal = txt->tam;
	char * ans;
	int j, i = 0;
	while(i < txt->tam){
		tamFinal += deltaChar((*txt->pointer)[i]);
		i++;
	}

	ans = malloc(tamFinal * sizeof(char));

	i = 0;
	j = 0;
	while(i < txt->tam){
		char * str;
		if ((*txt->pointer)[i] == ' '){
			str = "";
		}else if ((*txt->pointer)[i] == ','){
			str = "vr";
		}else if ((*txt->pointer)[i] == '.'){
			str = "pt";
		}else if ((*txt->pointer)[i] == ':'){
			str = "dp";
		}else if ((*txt->pointer)[i] == '!'){
			str = "ex";
		}else if ((*txt->pointer)[i] == '?'){
			str = "in";
		}else if ((*txt->pointer)[i] == '\n'){
			str = "nl";
		}else{
			ans[j] = (*txt->pointer)[i];
			j++;
			i++;
			continue;
		}
		substituiChar(str, &j, &ans);

		i++;
	}

	free(*txt->pointer);
	*txt->pointer = ans;
	txt->tam = tamFinal;
	txt->allocated = tamFinal;
}

void offsetChars(cesar * txt){
	int i = 0;

	while(i + 1 < txt->tam){
		char atual = (*txt->pointer)[i];
		char base;
		int offset;


		/* minusculas */
		if('a' <= atual && atual <= 'z'){
			base = 'a';
			offset = txt->offMin;
		}
		if('A' <= atual && atual <= 'Z'){
			base = 'A';
			offset = txt->offMai;
		}

		(*txt->pointer)[i] = base + ((atual - base) + offset) % 26;
		i++;
	}
}
